/*==================[inclusions]=============================================*/

#include "main.h"
#include "fsm_cafetera.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0;   // Para evTick1seg
    uint16_t cont_ms2 = 0;  // Para evTick500ms
    uint16_t cont_ms3 = 0;  // Para evTick1ms

    hw_Init();

    fsm_cafetera_init();

    while (input != EXIT) {
        input = hw_LeerEntrada();

        // En un microcontrolador estos eventos se generarian aprovechando las
        // interrupciones asociadas a los GPIO
        if (input == FICHA) {
            fsm_cafetera_raise_evFicha();
        }

        if (input == TE) {
            fsm_cafetera_raise_evTe();
        }

        if (input == CAFE) {
            fsm_cafetera_raise_evCafe();
        }
       

        // En un microcontrolador esto se implementaria en un handler de
        // interrupcion asociado a un timer
        cont_ms++;
        if (cont_ms == 1000) {
            cont_ms = 0;
            fsm_cafetera_raise_evTick1seg();
        }

        cont_ms2++;
        if (cont_ms2 == 500) {
            cont_ms2 = 0;
            fsm_cafetera_raise_evTick500ms();
        }

        cont_ms3++;
        if (cont_ms3 == 1) {
            cont_ms3 = 0;
            fsm_cafetera_raise_evTick1ms();
        }

        fsm_cafetera_runCycle();

        // Esta funcion hace que el sistema no responda a ningun evento por
        // 1 ms. Funciones bloqueantes de este estilo no suelen ser aceptables
        // en sistemas baremetal.
        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
