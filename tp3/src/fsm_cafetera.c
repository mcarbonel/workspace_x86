/*==================[inclusions]=============================================*/

#include "fsm_cafetera.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define ESPERA_SELECCIONANDO_SEG 30
#define ESPERA_ALERTA_SEG 2
#define ESPERA_TE_SEG 30
#define ESPERA_CAFE_SEG 45

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

FSM_CAFETERA_STATES_T state;

bool evFicha_raised;
bool evTe_raised;
bool evCafe_raised;
bool evTick1seg_raised;
bool evTick1ms_raised;
bool evTick500ms_raised;
uint8_t count_seg;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    evFicha_raised = 0;
    evTe_raised = 0;
    evCafe_raised = 0;
    evTick1seg_raised = 0;
    evTick1ms_raised = 0;
    evTick500ms_raised = 0;
}

/*==================[external functions definition]==========================*/

void fsm_cafetera_init(void)
{
    state = ESPERANDO;
    clearEvents();
}

void fsm_cafetera_runCycle(void)
{
    // El diagrama se encuentra en fsm_cafetera/media/diagrama_tp3.jpg
    switch (state) {
        case ESPERANDO:
            if (evFicha_raised) {
                state = SELECCIONANDO;
            }
            // PONER INSTRUCCION PARA DESTELLAR EL LED
            break;

        case SELECCIONANDO:
            if (evCafe_raised) {
                hw_Servir_Cafe_on();
                state = SIRVIENDO_CAFE;
                count_seg = 0;
            }
            else if (evTick1seg_raised && (count_seg < ESPERA_SELECCIONANDO_SEG)) {
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_SELECCIONANDO_SEG)) {
                hw_Devolver_Ficha();
                state = ESPERANDO;
            }
            else if (evTe_raised) {
                hw_Servir_Te_on();
                state = SIRVIENDO_TE;
                count_seg = 0;
            }
            break;

        case ALERTA_SONORA:
            if (evTick1seg_raised && (count_seg < ESPERA_ALERTA_SEG)) {
                count_seg++;
            } 
            else if (evTick1seg_raised && (count_seg == ESPERA_ALERTA_SEG)) {
                hw_ApagarAlerta();
                state = ESPERANDO;
            }
            break;

        case SIRVIENDO_TE:
            if (evTick1seg_raised && (count_seg < ESPERA_TE_SEG)) {
                count_seg++;
            }
            // PONER INSTRUCCION PARA HACER DESTELLAR EL LED
             else if (evTick1seg_raised && (count_seg == ESPERA_TE_SEG)) {
                hw_Servir_Te_off();
                state = ALERTA_SONORA;
                hw_ActivarAlerta();
                count_seg = 0;
            }
            break;

        case SIRVIENDO_CAFE:
            if (evTick1seg_raised && (count_seg < ESPERA_CAFE_SEG)) {
                count_seg++;
            }
            // PONER INSTRUCCION PARA HACER DESTELLAR EL LED
             else if (evTick1seg_raised && (count_seg == ESPERA_CAFE_SEG)) {
                hw_Servir_Cafe_off();
                state = ALERTA_SONORA;
                hw_ActivarAlerta();
                count_seg = 0;
            }  
            break;  
    }

    clearEvents();
}

void fsm_cafetera_raise_evFicha(void)
{
    evFicha_raised = 1;
}

void fsm_cafetera_raise_evTe(void)
{
    evTe_raised = 1;
}

void fsm_cafetera_raise_evCafe(void)
{
    evCafe_raised = 1;
}

void fsm_cafetera_raise_evTick1seg(void)
{
    evTick1seg_raised = 1;
}

void fsm_cafetera_raise_evTick1ms(void)
{
    evTick1ms_raised = 1;
}

void fsm_cafetera_raise_evTick500ms(void)
{
    evTick500ms_raised = 1;
}

void fsm_garaje_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
}

/*==================[end of file]============================================*/
