#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc
#define CAFE      49  // ASCII para la tecla 1
#define TE        50  // ASCII para la tecla 2 
#define FICHA     48  // ASCII para la tecla 0

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);

// Funciones basicas para leer que entrada esta activa y pausar la ejecucion
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_Servir_Te_on(void);
void hw_Servir_Cafe_on(void);
void hw_ActivarAlerta(void);
void hw_Servir_Te_off(void);
void hw_Servir_Cafe_off(void);
void hw_ApagarAlerta(void);
void hw_Devolver_Ficha(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */
