/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"
#include "stdio.h"
#include "time.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    uint8_t S1=0, S2=0, SEG=5;

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_1 && S1==0 && S2==0) {
            hw_AbrirBarrera(&S1, &S2);
            input = hw_LeerEntrada();
        }

        if (input == SENSOR_1 && S1==1 && S2==0) {
            hw_AbrirBarrera2(&S1, &S2);
            input = hw_LeerEntrada();
        }

        if (input == SENSOR_1 && S1==0 && S2==1) {
            hw_AbrirBarrera(&S1, &S2);
            input = hw_LeerEntrada();
        }

        if (input == SENSOR_2 && S1==0 && S2==1) {
            hw_SonarAlarma(&S1, &S2);
            printf("   La alarma se apaga en: \n");
            for(SEG=3; SEG>0; SEG--){
                  printf("             %d \n", SEG);
                  hw_Pausems(1000);
                } 
            printf("      Alarma apagada \n\n");
            input = hw_LeerEntrada();            
        }

        if (input == SENSOR_2 && S1==1 && S2==0) {             
            hw_Contar();
            if(input == SENSOR_2) {
                for(SEG=5; SEG>0; SEG--){
                  printf("              %d \n", SEG);
                  hw_Pausems(1000);
                  input = hw_LeerEntrada();
                } 
                //hw_CerrarBarrera(&S1, &S2);
            }
            /*if(input != SENSOR_2)*/else {
               hw_AbrirBarrera2(&S1, &S2);
            for(SEG=5; SEG>0; SEG--){
                  printf("             %d \n", SEG);
                  hw_Pausems(1000);
                  input = hw_LeerEntrada();
                } 
            }     
            hw_CerrarBarrera(&S1, &S2);
            input = hw_LeerEntrada();
        }

        if (input == SENSOR_2 && S1==0 && S2==0) {
            hw_SonarAlarma(&S1, &S2);
            printf("   La alarma se apaga en: \n");
            for(SEG=3; SEG>0; SEG--){
                  printf("             %d \n", SEG);
                  hw_Pausems(1000);
                } 
            printf("       Alarma apagada \n\n");
            input = hw_LeerEntrada();
        }

        hw_Pausems(100);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
