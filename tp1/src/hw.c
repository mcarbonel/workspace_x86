/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;
uint8_t SEG=5;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);       
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);    
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_AbrirBarrera(uint8_t* S1, uint8_t* S2)
{
    *S1=1;
    *S2=0;
    printf("      BARRERA ABIERTA \n\n");  
}

void hw_CerrarBarrera(uint8_t* S1, uint8_t* S2)
{
    *S1=0;
    *S2=0;
    //printf("Inicio conteo de 5 segundos \n\n");
    /* Acá tendría que hacer una lectura y si se activa el sensor_1 cancelar el cierre 
    de la barrera y reiniciar la cuenta de los 5 segundos */
    //hw_Pausems(5000);
    printf("\n      BARRERA CERRADA \n\n");
}

void hw_SonarAlarma(uint8_t* S1, uint8_t* S2)
{
    printf("      Alarma sonando \n");
    printf("   ES ENTRADA, NO SALIDA \n\n");
}

void hw_AbrirBarrera2(uint8_t* S1, uint8_t* S2)
{
    *S1=1;
    *S2=0;
    printf("Llegó un auto mientras la barrera estaba abierta \n\n");  
   // printf("      Reinicio la cuenta de los 5 segundos\n\n");
}

void hw_Contar(void)
{
    printf("Inicio cuenta de los 5 segundos \n\n");
    /*while(*input == SENSOR_2 && SEG>0) {
        for(SEG=5; SEG>0; SEG--){
         printf("\n %d Segundos", SEG);
         hw_Pausems(1000);
         input = hw_LeerEntrada();
        }
       // *S1=0;
       // *S2=0;
    }
    if(*input != SENSOR_2) {
        hw_AbrirBarrera2(&S1, &S2);
    } */
}
/*==================[end of file]============================================*/
